export default function({ app, route, req, redirect }) {
  const cookiesToken = app.$cookies.get('token')
  const isClient = process.browser
  const preRedir = '/user/login'
  let redirectURL = ''
  let path = ''
  // 在服务端
  // 在客户端判读是否需要登陆
  if (isClient) {
    path = route.path
  } else {
    path = req.originalUrl
  }
  if (path) {
    redirectURL = preRedir + '?ref=' + encodeURIComponent(path)
  }
  // 需要进行权限判断的页面开头
  if (!cookiesToken && route.path !== preRedir) {
    redirect(redirectURL)
  }
  if (isClient) {
    console.log(1222)
    app.router.beforeEach((to, from, next) => {
      console.log(to)
      if (cookiesToken) {
        next()
      } else if (to.path === 'user/login') {
        next()
      } else {
        console.log(222)
        next(redirectURL)
      }
    })
  }
}
