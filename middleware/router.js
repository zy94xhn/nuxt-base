export default (ctx) => {
  const { app, store } = ctx
  const cookies = process.browser ? ctx.$cookies : app.$cookies
  console.log(store)
  app.router.beforeEach((to, from, next) => {
    // 设置条件
    const cookiesToken = cookies.get('token')
    console.log(cookiesToken)
    next()
  })
}
