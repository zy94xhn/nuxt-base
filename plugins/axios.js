import qs from 'qs'
export default function({ app: { $axios, $cookies }}) {
  $axios.defaults.baseURL = 'http://apiccp.hb.com/v1.1/'
  $axios.defaults.timeout = 30000
  $axios.interceptors.request.use((config) => {
    config.headers.Accept = '*/*'
    config.headers['Content-Type'] =
      'application/x-www-form-urlencoded;charset=UTF-8'
    config.dataType = 'json'
    config.responseType = 'text'
    config.url = `${config.url}`
    config.transformRequest = function(data) {
      return qs.stringify(Object.assign(data || {}, Object))
    }
    // 头部带上验证信息
    // config.headers['X-Token'] = $cookies.get('token') || ''
    // config.headers['X-Device-Id'] = $cookies.get('clientId') || ''
    // config.headers['X-Uid'] = $cookies.get('userId') || ''
    return config
  })
  $axios.interceptors.response.use((response) => {
    if (/^[4|5]/.test(response.status)) {
      return Promise.reject(response.statusText)
    }
    return response.data
  })
}
