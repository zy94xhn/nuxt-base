const actions = {
  nuxtServerInit({ commit }, ctx) {
    // 加载数据到状态树
    const { app } = ctx
    console.log(app)

    const cookies = process.browser ? ctx.$cookies : app.$cookies
    console.log(cookies.get('token'))
    commit('user/SET_TOKEN', app.$cookies.get('token'))
  }
}

export default {
  actions
}
