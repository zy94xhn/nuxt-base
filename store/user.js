export const state = () => ({
  token: '',
  userInfo: {}
})
export const getters = {}

export const actions = {}

export const mutations = {
  SET_TOKEN(state, token) {
    state.token = token
  },
  SET_USERINFO(state, data) {
    state.userInfo = data
  }
}
