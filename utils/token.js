const key = 'token'

export function setToken(app, token) {
  app.$cookies.set(key, token, { path: '/' })
}

export function getToken(app) {
  return app.$cookies.get(key)
}
