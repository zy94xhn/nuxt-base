const setAuthInfo = (ctx, res) => {
  let $cookies, $store
  // 客户端
  if (process.client) {
    $cookies = ctx.$cookies
    $store = ctx.$store
  }
  // 服务端
  if (process.server) {
    $cookies = ctx.app.$cookies
    $store = ctx.store
  }
  if ($cookies && $store) {
    // 过期时长 new Date(Date.now() + 8.64e7 * 365 * 10)
    const expires = new Date(Date.now() + 8.64e7 * 365 * 10)
    // 设置cookie
    $cookies.set('token', res.token, { expires, path: '/' })
    // 设置vuex
    $store.commit('user/SET_TOKEN', res.token)
  }
}
export { setAuthInfo }
